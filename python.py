import psycopg2
import psycopg2.extras

from flask import Flask, render_template,request

app = Flask(__name__)

@app.route('/') #render hime template
def home():
    return render_template('home.html')
    
def getConn(): #function to get connection to database for reusability 
    pwFile = open("pw.txt", "r")
    pw = pwFile.read();
    pwFile.close()
    connStr="host='cmpstudb-01.cmp.uea.ac.uk' \
               dbname= 'ajc17dru' user='ajc17dru' password = " + pw
    conn=psycopg2.connect(connStr) 
    return conn
    


@app.route('/task1', methods=['POST']) #Insert customer method
def task1():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        customerid = int(request.form['customerid'])
        name = request.form['name']
        email = request.form['email']
        
        cur.execute('INSERT INTO customer (customerid,name,email) VALUES (%s,%s,%s)',\
        [customerid,name,email])
        conn.commit()
        
        return render_template('home.html', msg1 = 'Record added')
    except Exception as e:
        return render_template('home.html', msg1 = 'INSERT ABORTED', error1 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task2', methods=['POST']) #Create ticket method
def task2():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        priority = int(request.form['priority'])
        ticketid = int(request.form['ticketid'])
        problem = request.form['problem']
        status = request.form['status']
        customerid = int(request.form['customerid'])
        productid = int(request.form['productid'])

        cur.execute('INSERT INTO ticket (ticketid,problem,status,priority,loggedtime,customerid,productid) \
        VALUES (%s,%s,%s,%s,CURRENT_TIMESTAMP,%s,%s)',[ticketid,problem,status,priority, customerid,productid])
        conn.commit()
        cur.execute('SELECT * FROM ticket WHERE(ticketid = %s)',[ticketid])

        rows = cur.fetchall()
        if rows:
            return render_template('task2.html', rows = rows)
        else:
            return render_template('task2.html)', msg1 = "No data found.")
    except Exception as e:
        return render_template('home.html', msg2 = 'INSERT ABORTED', error2 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task3', methods=['POST']) #Create ticket update method
def task3():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        ticketupdateid = int(request.form['ticketupdateid'])
        message = request.form['message']
        ticketid = int(request.form['ticketid'])
        staffid = int(request.form['staffid'])
        
        cur.execute('INSERT INTO ticketupdate (ticketupdateid,message,ticketid,updatetime,staffid) VALUES (%s,%s,%s,CURRENT_TIMESTAMP,%s)',\
            [ticketupdateid,message,ticketid,staffid])

        conn.commit()
        return render_template('home.html', msg3 = 'Record added')
    except Exception as e:
        return render_template('home.html', msg3 = 'INSERT ABORTED', error3 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task4', methods=['GET']) #get all open tickets method
def task4():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        cur.execute("SELECT ticket.ticketid,ticketupdate.updatetime FROM ticket,ticketupdate WHERE ((ticket.status = 'open') AND updatetime = ( SELECT MAX(updatetime) FROM ticketupdate WHERE (ticket.ticketid = ticketupdate.ticketid)))")
        rows = cur.fetchall()
        if rows:
            return render_template('task4.html', rows = rows)
        else:
            return render_template('task4.html)', msg1 = "No data found.")
    except Exception as e:
        return render_template('home.html', msg4 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task5',methods=['POST']) #close ticket methyod
def task5():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        ticketid = int(request.form['ticketidupdate'])
        
        cur.execute("UPDATE ticket SET status = 'closed' WHERE (ticketid = %s)", [ticketid])
        conn.commit()
        return render_template('home.html',msg5 = 'Ticket closed')
    except Exception as e:
        return render_template('home.html', msg5= "Update failed.",error5 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task6', methods=['GET']) #list all updates to ticket method
def task6():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        ticketid2 = int(request.args['ticketidupdates'])
        print("test")
        cur.execute("SELECT ticket.problem,ticketupdate.updatetime,ticketupdate.message,staff.name FROM ticket,staff,ticketupdate \
        WHERE (ticket.ticketid = %s AND ticket.ticketid = ticketupdate.ticketid AND ticketupdate.staffid = staff.staffid) \
        ORDER BY ticket.problem, ticketupdate.updatetime", [ticketid2])
        print("test")
        rows = cur.fetchall()
        if rows:
            return render_template('task6.html',rows = rows)
        else: 
            return render_template('task6.html', msg1 = 'No data found.')
    except Exception as e:
        return render_template('home.html', msg6 = e)
    finally:
        if conn:
            conn.close()
@app.route('/task7', methods = ['GET']) #Closed ticket status report method
def task7():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        cur.execute('SELECT total.ticketid,lastupdate,firstupdate,updates FROM MaxTicketDate last, MinTicketDate first, TicketUpdateTotal total \
        WHERE (total.ticketid = last.ticketid AND total.ticketid = first.ticketid)')
        rows = cur.fetchall()
        if rows:
            return render_template('task7.html', rows = rows)
        else:
            return render_temlpate('task6.html',row = "No data found.")
    except Exception as e:
        return render_template('home.html', msg7 = e)
    finally:
        if conn:
            conn.close()

@app.route('/task8',methods = ['POST']) #delete customer method
def task8():
    try:
        conn = None
        conn = getConn()
        cur = conn.cursor()
        cur.execute('SET SEARCH_PATH TO database')
        
        ticketid = int(request.form['ticketid'])
        
        cur.execute("DELETE FROM customer WHERE customerid= %s",[ticketid])
        conn.commit()
        
        return render_template('home.html', msg8 = "Customer deleted.")
    except Exception as e:
        return render_template('home.html', msg8 = "Deletion failed.", error8 = e)
            
if __name__ == '__main__':
    app.run(debug = True)